const width = 300;
const height = 300;
const gridSize = 5;
const incr = width / (2 * gridSize);
const origin = gridSize * incr;
const arrowSize = 20;

grid = [];
vecA = null;
vecB = null;

function checkLineInteraction(x, y, mx, my) {
    buffer = 0.2;
    d1 = dist(0, 0, mx, my);
    d2 = dist(x, y, mx, my);
    lineLen = dist(0, 0, x, y);

    if (d1 + d2 >= lineLen - buffer && d1 + d2 <= lineLen + buffer)
        return true;
    return false;
}

function checkInteraction(vec) {
    vec.weight = 6;

    if (checkLineInteraction(vec.pos.x, vec.pos.y, mx, my))
        vec.weight = 10;
    
    if (vec.pressed)
        vec.weight = 10;
}

function drawGrid() {
    strokeWeight(1);
    grid.forEach(element => 
        line(0, element, width, element)
    );
    grid.forEach(element => 
        line(element, 0, element, height)
    );

    strokeWeight(3);
    line(0, origin, width, origin);
    line(origin, 0, origin, height);
}

function drawVector(vector) {
    push();
    strokeWeight(vector.weight);
    stroke(vector.color)
    translate(origin, origin);
    line(0, 0, vector.pos.x, -vector.pos.y);
    rotate(-vector.pos.heading());
    translate(vector.pos.mag() - arrowSize, 0);
    line(0, arrowSize / 2, arrowSize, 0);
    line(0, -arrowSize / 2, arrowSize, 0);
    pop();
}

function drawDashedVector(x, y, vec, col) {
    push();
    stroke(col);
    drawingContext.setLineDash([5,15]);
    translate(origin + x, origin - y);
    line(0, 0, vec.pos.x, -vec.pos.y);
    pop();
}

function drawVectors() {
    checkInteraction(vecA);
    checkInteraction(vecB);
    drawVector(vecA);
    drawVector(vecB);

    vecSum = {
        pos : createVector(vecA.pos.x + vecB.pos.x, vecA.pos.y + vecB.pos.y),
        weight : 6,
        color : color(0, 0, 0)
    };
    drawVector(vecSum);

    drawDashedVector(vecA.pos.x, vecA.pos.y, vecB, color(51, 153, 255));
    drawDashedVector(vecB.pos.x, vecB.pos.y, vecA, color(102, 255, 102));
}



function setup() {
    console.log(incr);
    createCanvas(width, height);
    for (loc = 0; loc < width; loc += incr)
        grid.push(loc);

    vecA = {
        pos: createVector(1 * incr, 3 * incr),
        weight : 6,
        pressed : false,
        color : color(102, 255, 102)
    }
    vecB = {
        pos: createVector(3 * incr, 1 * incr),
        weight : 6,
        pressed : false,
        color : color(51, 153, 255)
    }
}

function draw() {
    mx = mouseX - origin;
    my = -(mouseY - origin);

    background(250);
    drawGrid();
    drawVectors();
}

function mouseDragged() {
    if (vecA.pressed) {
        console.log("moving");
        vecA.pos.x = mx;
        vecA.pos.y = my;
    } else if (vecB.pressed) {
        console.log("moving");
        vecB.pos.x = mx;
        vecB.pos.y = my;
    }
}

function mousePressed() {
    if (checkLineInteraction(vecA.pos.x, vecA.pos.y, mx, my)) {
        console.log("Vec A Pressed");
        vecA.pos.x = mx;
        vecA.pos.y = my;
        vecA.pressed = true;
    } else if (checkLineInteraction(vecB.pos.x, vecB.pos.y, mx, my)) {
        console.log("Vec B Pressed");
        vecB.pos.x = mx;
        vecB.pos.y = my;
        vecB.pressed = true;
    }
}

function mouseReleased() {
    if (vecA.pressed) {
        vecA.pressed = false;
    }
    if (vecB.pressed) {
        vecB.pressed = false;
    }
}